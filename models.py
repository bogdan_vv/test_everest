
from flask_security import RoleMixin, UserMixin
from flask_sqlalchemy import SQLAlchemy
from enum import Enum as EnumType
from sqlalchemy import Enum

db = SQLAlchemy()


roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('users.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('roles.id'))
)


class Role(db.Model, RoleMixin):
    __tablename__ = 'roles'
    
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'<Role {self.name}>'


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return self.email

    def __repr__(self):
        return f'<User {self.name}>'
    
class Product(db.Model):
    __tablename__ = 'products'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    color = db.Column(db.String(50))
    weight = db.Column(db.Float)
    price = db.Column(db.Float)

    def __str__(self):
        return self.name


class Address(db.Model):
    __tablename__ = 'addresses'

    id = db.Column(db.Integer, primary_key=True)
    country = db.Column(db.String(100))
    city = db.Column(db.String(100))
    street = db.Column(db.String(255))

    def __str__(self):
        return f'country:{self.country}, city:{self.city}, street:{self.street},'


class OrderStatus(EnumType):
    EXECUTED = 'виконано'
    CANCELED = 'відмнено'
    PROCESSING = 'обробляється'


class Order(db.Model):
    __tablename__ = 'orders'

    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
    address_id = db.Column(db.Integer, db.ForeignKey('addresses.id'))
    quantity = db.Column(db.Integer)
    status = db.Column(Enum(OrderStatus), default=OrderStatus.PROCESSING)

    product = db.relationship('Product', backref='orders')
    address = db.relationship('Address', backref='orders')

    def __str__(self):
        return f'{self.status}'