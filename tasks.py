from celery import Celery


celery = Celery('tasks', broker='redis://localhost:6379/0')

@celery.task
def track_status_change(order_id, new_status):
    with open('order_events.log', 'a') as log_file:
        log_file.write(f'Замовлення {order_id} змінило статус на {new_status}\n')