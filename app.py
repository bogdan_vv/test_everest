from flask import Flask, redirect, url_for
from flask_migrate import Migrate
from flask_security import SQLAlchemyUserDatastore, Security
from base_auth import AuthorizationView
from flask_admin import Admin
from models import db, User, Role, Product, Address, Order
from admin import HomeAdminView, BaseAdminView, ProductAdminView, AdressAdminView, OrderAdminView
from flask_jsonrpc import JSONRPC
import os


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@host.docker.internal:3308/everest_db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECURITY_PASSWORD_SALT'] = 'salt'
app.secret_key = 'key'

db.init_app(app)
migrate = Migrate(app, db)

jsonrpc = JSONRPC(app, '/api', jsonrpc_site_api=AuthorizationView)

admin = Admin(app, name='Admin', index_view=HomeAdminView(), template_mode='bootstrap3')
admin.add_view(ProductAdminView(Product, db.session))
admin.add_view(AdressAdminView(Address, db.session))
admin.add_view(OrderAdminView(Order, db.session))
admin.add_view(BaseAdminView(User, db.session))
admin.add_view(BaseAdminView(Role, db.session))

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

@app.route('/')
def index():
    return redirect(url_for('admin.index'))

@jsonrpc.method('get.order')
def get_order_status(order_id: int) -> tuple:
    order = Order.query.get(order_id)
    if order is None:
        return {'message': 'Order not found'}, 404
    return {
        'order_id': order.id,
        'status': order.status.name
    }, 200


if __name__ == '__main__':
    app.run(debug=True)
