# test_everest



## Getting started


```
git clone https://gitlab.com/bogdan_vv/test_everest

docker-compose up --build

docker exec -i everest-app flask db upgrade

cat dump_everest_db.sql | docker exec -i everest-db /usr/bin/mysql -u root --password=root everest_db
```
## For Admin
## User with ADMIN role

## !!! if you change the order status in the project directory, order_events.log should be created

```
admin@gmail.com
admin
```

## User with USER role

```
user@gmail.com
user
```

## Get order status from api

without authorization

```
curl -i -X POST -H "Content-Type: application/json" \
  -d '{
    "jsonrpc": "2.0",
    "method": "get.order",
    "params": {
        "order_id": 1
    },
    "id": "1"
  }' http://localhost:5000/api
```

with authorization

```
curl -i -X POST -H "Content-Type: application/json" \
  -H 'X-Username: username' \
  -H 'X-Password: secret' \
  -d '{
    "jsonrpc": "2.0",
    "method": "get.order",
    "params": {
        "order_id": 1
    },
    "id": "1"
  }' http://localhost:5000/api
```

