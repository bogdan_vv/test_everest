from flask import url_for, redirect, request, abort
from flask_admin import AdminIndexView
from flask_security import current_user
from flask_admin.contrib.sqla import ModelView
from flask_admin.form import Select2Field
from sqlalchemy import event
from sqlalchemy.orm import Session, attributes
from tasks import  track_status_change
from models import Order

class HomeAdminView(AdminIndexView):
    def is_accessible(self):
        return (current_user.is_active and
                current_user.is_authenticated and
                current_user.has_role('admin')
                )

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))


class BaseAdminView(ModelView):
    def is_accessible(self):
        return (current_user.is_active and
                current_user.is_authenticated and
                current_user.has_role('admin')
                )

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))

class ProductAdminView(BaseAdminView):
    form_excluded_columns = ('orders',)


class AdressAdminView(BaseAdminView):
    form_excluded_columns = ('orders',)


class OrderAdminView(BaseAdminView):

    @event.listens_for(Order, 'before_update')
    def before_update_listener(mapper, connection, target):
        session = Session.object_session(target)
        if not session.is_modified(target, include_collections=False):
            return

        old_status = attributes.get_history(target, 'status')[2][0]  
        new_status = target.status

        if old_status != new_status:
            track_status_change.apply_async((target.id, new_status))

    form_overrides = {
        'status': Select2Field
    }

    form_args = {
        'status': {
            'choices': [
                ('EXECUTED', 'виконано'),
                ('CANCELED', 'відмнено'),
                ('PROCESSING', 'обробляється')
            ]
        }
    }
