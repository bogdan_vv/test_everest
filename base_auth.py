from flask import request
from flask_jsonrpc import JSONRPCView



class UnauthorizedError(Exception):
    pass


class AuthorizationView(JSONRPCView):
    def check_auth(self) -> bool:
        username = request.headers.get('X-Username')
        password = request.headers.get('X-Password')
        return username == 'username' and password == 'secret'

    def dispatch_request(self):
        if not self.check_auth():
            raise UnauthorizedError()
        return super().dispatch_request()